package es.iesnervion.miguel.ejemplofragments;

import androidx.lifecycle.ViewModel;

public class MainViewModel extends ViewModel {
    private boolean tablet;

    public boolean isTablet() {
        return tablet;
    }

    public void setTablet(boolean tablet) {
        this.tablet = tablet;
    }


}
