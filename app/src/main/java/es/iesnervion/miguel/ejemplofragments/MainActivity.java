package es.iesnervion.miguel.ejemplofragments;


import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

public class MainActivity extends AppCompatActivity implements Navegacion.OnFragmentInteractionListener {

    ViewModel mainViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View contenedorGen;
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        ((MainViewModel) mainViewModel).setTablet(true);
        contenedorGen=findViewById(R.id.contenedorGeneral);
        if (contenedorGen!=null){
            Navegacion frag = new Navegacion();
            getSupportFragmentManager().beginTransaction()
                                       .add(R.id.contenedorGeneral,frag)
                                       .addToBackStack(null)
                                       .commit();
            ((MainViewModel) mainViewModel).setTablet(false);
        }

    }

    @Override
    public void onFragmentInteraction(View v) {
        String texto = "";
        switch (v.getId()){
            case R.id.boton1:
                texto = "Esto es el detalle del BOTÓN 1";
                break;
            case R.id.boton2:
                texto = "Esto es el detalle del BOTÓN 2";
                break;
        }
        DetalleFragment frag = DetalleFragment.newInstance(texto);
        if (((MainViewModel) mainViewModel).isTablet()){

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.detalle,frag)
                    .addToBackStack(null)
                    .commit();
        }
        else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contenedorGeneral,frag)
                    .addToBackStack(null)
                    .commit();
        }

    }

}
